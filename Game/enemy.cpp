#include "enemy.h"
#include "sprite.h"

//generator
mt19937 gen(time(0));
uniform_int_distribution<int> urd(1, 2);

Enemy::Enemy(Sprite *sprite): Drawable(), Movable(), KeyboardEventListener(){
    this->sprite = sprite;
    this->timeInCurrentState = 0;
}

Enemy::~Enemy(){
    delete sprite;
}

void Enemy::setBackgroundImage(Level *backgroundImage){
    this->backgroundImage = backgroundImage;
}

Sprite * Enemy::getSprite(){
    return sprite;
}

void Enemy::attack(Player *player){
    if(player->getHealth() > 0){
        if(player->getSprite()->getSpriteRect()->y + player->getSprite()->getSpriteRect()->h >= sprite->getSpriteRect()->y + sprite->getSpriteRect()->h/2 &&
           player->getSprite()->getSpriteRect()->y + player->getSprite()->getSpriteRect()->h <= sprite->getSpriteRect()->y + sprite->getSpriteRect()->h &&
           player->getSprite()->getSpriteRect()->x + player->getSprite()->getSpriteRect()->w >= sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/4 + 16/4 &&
           player->getSprite()->getSpriteRect()->x <= sprite->getSpriteRect()->x + sprite->getSpriteRect()->w - sprite->getSpriteRect()->w/4 - 16/4){
            Mix_PlayChannel(4, backgroundImage->getSoundEffect("1-up"), 0);
            player->setHealth(player->getHealth() - 1);
            player->setCoins(player->getCoins() - 1);
            player->setPoints(player->getPoints() - 150);
            if(player->getHealth() > 0){
                /// y mora biti paran
                player->getSprite()->getSpriteRect()->y = 200;
            };
        };
    };
}

void Enemy::randomChangeStates(){
    if(sprite->getOnTheGround()){

        if(timeInCurrentState == 0){
            sprite->setState(urd(gen));
        };

        if(timeInCurrentState <= 150){
            timeInCurrentState++;
        } else {
            timeInCurrentState = 0;
        };

    };
}

void Enemy::move(int dx, int dy){
    sprite->move(dx, dy);
}

void Enemy::move(){
    if(!backgroundImage->getMoving()){
        // POZADINA STOJI
        if(sprite->getState() != Sprite::STOP){
            // ENEMY SE KRECE U DESNO
            if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + 2*sprite->getSpriteRect()->w < backgroundImage->getX() + 5120){
                move(1, 0);
            } else {
                sprite->setState(Sprite::LEFT);
            };
            // ENEMY SE KRECE U LEVO
            if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x - sprite->getSpriteRect()->w > backgroundImage->getX()){
                move(-1, 0);
            } else {
                sprite->setState(Sprite::RIGHT);
            };
        };
    } else {
        if(sprite->getState() != Sprite::STOP){
            if(backgroundImage->getState() == Level::LEFT){
                // POZADINA SE KRECE U LEVO

                // ENEMY SE KRECE U DESNO
                if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + 2*sprite->getSpriteRect()->w < backgroundImage->getX() + 5120){
                    move(1, 0);
                } else {
                    sprite->setState(Sprite::LEFT);
                };
                // ENEMY SE KRECE U LEVO
                if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x - sprite->getSpriteRect()->w > backgroundImage->getX()){
                    move(-4, 0);
                } else {
                    sprite->setState(Sprite::RIGHT);
                };
            } else if(backgroundImage->getState() == Level::RIGHT) {
                // POZADINA SE KRECE U DESNO

                // ENEMY SE KRECE U DESNO
                if(sprite->getState()&Sprite::RIGHT && sprite->getSpriteRect()->x + 2*sprite->getSpriteRect()->w < backgroundImage->getX() + 5120){
                    move(4, 0);
                } else {
                    sprite->setState(Sprite::LEFT);
                };
                // ENEMY SE KRECE U LEVO
                if(sprite->getState()&Sprite::LEFT && sprite->getSpriteRect()->x - sprite->getSpriteRect()->w > backgroundImage->getX()){
                    move(-1, 0);
                } else {
                    sprite->setState(Sprite::RIGHT);
                };
            };
        };
    };
}

void Enemy::draw(SDL_Renderer *renderer){
    if(sprite->getState()&Sprite::LEFT){
        sprite->getSpriteSheet()->drawFrame("walk_left", sprite->getCurrentFrame(), sprite->getSpriteRect(), renderer);
    } else if(sprite->getState()&Sprite::RIGHT){
        sprite->getSpriteSheet()->drawFrame("walk_right", sprite->getCurrentFrame(), sprite->getSpriteRect(), renderer);
    } else if(sprite->getState() == Sprite::STOP){
        sprite->getSpriteSheet()->drawFrame("stop", 0, sprite->getSpriteRect(), renderer);
    };

    sprite->setFrameCounter(sprite->getFrameCounter() + 1);
    if(sprite->getFrameCounter()%sprite->getFrameSkip() == 0){
        sprite->setCurrentFrame(sprite->getCurrentFrame() + 1);
        if(sprite->getCurrentFrame() >= 2){
            sprite->setCurrentFrame(0);
        };
        sprite->setFrameCounter(0);
    };

}


