#include "sprite.h"

Sprite::Sprite(SpriteSheet *sheet, int x, int y, int width, int height): Drawable(), Movable(){
    state = Sprite::STOP;
    lastState = state;
    this->sheet = sheet;
    currentFrame = 0;
    frameCounter = 0;
    frameSkip = 1;

    spriteRect = new SDL_Rect();
    spriteRect->x = x;
    spriteRect->y = y;
    spriteRect->w = width;
    spriteRect->h = height;
}

Sprite::~Sprite(){
    delete sheet;
    delete spriteRect;
}

int Sprite::getFrameSkip(){
    return frameSkip;
}

void Sprite::setFrameSkip(int frameSkip){
    if(frameSkip < 0){
        frameSkip = 0;
    }
    this->frameSkip = frameSkip + 1;
}

short int Sprite::getState(){
    return state;
}

void Sprite::setState(short int state){
    this->state = state;
}

short int Sprite::getLastState(){
    return lastState;
}

void Sprite::setLastState(short int lastState){
    this->lastState = lastState;
}

void Sprite::draw(SDL_Renderer *renderer){
    if(state == LEFT_DOWN && onTheGround){
        sheet->drawFrame("look_down_left", 0, spriteRect, renderer);
    } else if(state == RIGHT_DOWN && onTheGround){
        sheet->drawFrame("look_down_right", 0, spriteRect, renderer);
    } else if(state == LEFT_UP && onTheGround){
        sheet->drawFrame("look_up_left", 0, spriteRect, renderer);
    } else if(state == RIGHT_UP && onTheGround){
        sheet->drawFrame("look_up_right", 0, spriteRect, renderer);
    } else if(state&LEFT){
        sheet->drawFrame("walk_left", currentFrame, spriteRect, renderer);
    } else if(state&RIGHT){
        sheet->drawFrame("walk_right", currentFrame, spriteRect, renderer);
    } else if(state&UP){
        sheet->drawFrame("walk_right", currentFrame, spriteRect, renderer);
    } else if(state&VIEW){
        sheet->drawFrame("look_up", 0, spriteRect, renderer);
    } else if(state&CROUCH){
        sheet->drawFrame("crouch", 0, spriteRect, renderer);
    } else if(state == STOP){
        if(lastState&LEFT){
            sheet->drawFrame("stop_left", 0, spriteRect, renderer);
        } else if(lastState&RIGHT){
            sheet->drawFrame("stop_right", 0, spriteRect, renderer);
        } else if(lastState&VIEW){
            sheet->drawFrame("look_up", 0, spriteRect, renderer);
        } else {
            sheet->drawFrame("stop", 0, spriteRect, renderer);
        }
    }

    frameCounter++;
    if(frameCounter%frameSkip == 0){
        currentFrame++;
        if(currentFrame >= 5){
            currentFrame = 0;
        }
        frameCounter = 0;
    }
}

void Sprite::move(int dx, int dy){
    spriteRect->x += dx;
    spriteRect->y += dy;
}

void Sprite::move(){
    if(state != STOP){
        if(state&LEFT){
            move(-1, 0);
        };
        if(state&RIGHT){
            move(1, 0);
        };
        if(state&UP){
            move(0, -8);
        };
    };
}

SpriteSheet * Sprite::getSpriteSheet(){
    return sheet;
}

int Sprite::getCurrentFrame(){
    return currentFrame;
}

void Sprite::setCurrentFrame(int currentFrame){
    this->currentFrame = currentFrame;
}

int Sprite::getFrameCounter(){
    return frameCounter;
}

void Sprite::setFrameCounter(int frameCounter){
    this->frameCounter = frameCounter;
};

void Sprite::setOnTheGround(bool onTheGround){
    this->onTheGround = onTheGround;
}

bool Sprite::getOnTheGround(){
    return onTheGround;
}
