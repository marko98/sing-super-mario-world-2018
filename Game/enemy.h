#ifndef ENEMY_H_INCLUDED
#define ENEMY_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>

#include <iostream>
#include <random>
#include <ctime>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"
#include "level.h"

class Sprite;

using namespace std;

class Enemy: public Drawable, public Movable, public KeyboardEventListener{
private:
    Sprite *sprite;
    Level *backgroundImage;
    int timeInCurrentState;
public:
    Enemy(Sprite *sprite);
    virtual ~Enemy();

    void setBackgroundImage(Level *backgroundImage);
    void randomChangeStates();
    Sprite * getSprite();
    void attack(Player *player);

    virtual void draw(SDL_Renderer *renderer);
    virtual void move(int dx, int dy);
    virtual void move();
    virtual void listen(SDL_KeyboardEvent &event){};
};

#endif // ENEMY_H_INCLUDED
