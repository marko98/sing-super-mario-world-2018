#ifndef TILESET_H_INCLUDED
#define TILESET_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "tile.h"

using namespace std;

class Tileset{
private:
    map<string, Tile*> tileset;
    SDL_Texture *texture;
    SDL_Rect *dest;
public:
    Tileset(istream &inputStream, SDL_Renderer *renderer);
    virtual ~Tileset();

    void draw(string code, int x, int y, SDL_Renderer *renderer);
    map<string, Tile*> getTileset();
};

#endif // TILESET_H_INCLUDED
