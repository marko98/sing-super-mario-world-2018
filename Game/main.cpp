#include <iostream>

#include "engine.h"

using namespace std;

int main(int argc, char** argv)
{
    Engine *engine = new Engine("Super Mario World");

    /// 1. BACKGROUND IMAGE
    engine->addTileset("resources/tilesets/tilesets_level0_template.txt", "level0_background_image");
    engine->addLevel("resources/levels/level0_template_tile.txt", "level0_background_image", 0, 0, 16, 16);

    /// 2. BACKGROUND TILES
    engine->addTileset("resources/tilesets/tilesets_level0.txt", "level0_tiles");
    engine->addLevel("resources/levels/level0_tiles.txt", "level0_tiles", 0, 0, 16, 16);

    /// 3. GENERAL TILES
    engine->addTileset("resources/tilesets/general_tiles.txt", "general_tiles");
    engine->addLevel("resources/levels/general_tiles.txt", "general_tiles", 0, 0, 16, 16);

    /// 4. ADD MUSIC
    engine->addBackgroundMusic("resources/sounds/Super Mario World - Overworld Theme Music.mp3");

    /// 5. ADD COINS
    engine->addCoin("resources/tilesets/coin_sprite_sheet.txt", 6, 1145, 2);

    /// 6. ADD ENEMIES
    ///-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 3, 700);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 3, 800);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 3, 5000);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 3, 3000);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 3, 100);
    engine->addEnemy("resources/creatures/green_turtle_sprite_sheet.txt", 3, 800);

    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 3, 1520);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 3, 3580);
    //-------------------------------------------------------------------------------------
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 3, 3500);
    engine->addEnemy("resources/creatures/red_turtle_sprite_sheet.txt", 3, 4100);

    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 3, 2000);
    engine->addEnemy("resources/creatures/purple_turtle_sprite_sheet.txt", 3, 2600);
    ///-------------------------------------------------------------------------------------


    /// 7. ADD LEAD PLAYER
    engine->addPlayer("resources/creatures/mario_sprite_sheet.txt", 3, engine);

    engine->run();
    delete engine;

    return 0;
}
