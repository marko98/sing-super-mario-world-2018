#ifndef SPRITESHEET_H_INCLUDED
#define SPRITESHEET_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "tile.h"

using namespace std;

class SpriteSheet{
public:
    typedef Tile Frame;
    typedef vector<Frame*> Frames;
private:
    SDL_Texture *texture;
    map<string, Frames> animations;
public:
    SpriteSheet(istream &inputStream, SDL_Renderer *renderer);
    virtual ~SpriteSheet();
    void drawFrame(string animation, int frame, SDL_Rect *dest, SDL_Renderer *renderer);
};

#endif // SPRITESHEET_H_INCLUDED
