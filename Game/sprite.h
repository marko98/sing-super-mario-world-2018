#ifndef SPRITE_H_INCLUDED
#define SPRITE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "level.h"
#include "player.h"

using namespace std;

class Sprite: public Drawable, public Movable{
public:
    enum State: short int{STOP=0, LEFT=1, RIGHT=2, UP=4, CROUCH=8, VIEW=16,
                        LEFT_UP=LEFT|VIEW, LEFT_DOWN=LEFT|CROUCH,
                        RIGHT_UP=RIGHT|VIEW, RIGHT_DOWN=RIGHT|CROUCH};
private:
    short int state;
    short int lastState;
    SpriteSheet *sheet;
    SDL_Rect *spriteRect;
    int currentFrame, frameCounter, frameSkip;
    bool onTheGround = false;
public:
    /// y koordinata mora biti parna
    Sprite(SpriteSheet *sheet, int x=0, int y=320, int width=32, int height=32);
    virtual ~Sprite();
    SpriteSheet * getSpriteSheet();
    int getCurrentFrame();
    void setCurrentFrame(int currentFrame);
    int getFrameCounter();
    void setFrameCounter(int frameCounter);
    int getFrameSkip();
    void setFrameSkip(int frameSkip);
    short int getState();
    void setState(short int state);
    short int getLastState();
    void setLastState(short int lastState);
    void setOnTheGround(bool onTheGround);
    bool getOnTheGround();

    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    SDL_Rect * getSpriteRect(){ return spriteRect; };
    friend ostream& operator<<(ostream&, const Sprite&);
};

struct Gravity{
    Sprite *sprite = nullptr;
    Level *backgroundImage = nullptr;
    Level *backgroundTiles = nullptr;
    Level *generalTiles = nullptr;
    Player *player = nullptr;
    int plocice = 0;

    Gravity(Level *backgroundImage, Level *backgroundTiles, Level *generalTiles, Sprite *sprite): backgroundImage(backgroundImage), backgroundTiles(backgroundTiles), generalTiles(generalTiles), sprite(sprite){};
    Gravity(Level *backgroundImage, Level *backgroundTiles, Level *generalTiles, Player *player): backgroundImage(backgroundImage), backgroundTiles(backgroundTiles), generalTiles(generalTiles), sprite(player->getSprite()), player(player){};
    virtual ~Gravity(){
        delete sprite;
        delete player;
    };

    Sprite *getSprite(){ return sprite; };

    void gravity(){
        int xCoordinate = sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2;
        int yCoordinate = sprite->getSpriteRect()->y + sprite->getSpriteRect()->h;

        /// COLUMNS-----------------------------
        int column = (xCoordinate - generalTiles->getX())/16;
        if(xCoordinate%16 == 0){
            column -= 1;
        }
        if(sprite->getSpriteRect()->x == 0){
            column = 0;
        }
        ///-----------------------------------

        /// ROWS--------------------------------
        int row = yCoordinate/16;
        if(yCoordinate%16 == 0){
            row -= 1;
        };
        ///-----------------------------------

        //cout << "Sprite x: " << sprite->getSpriteRect()->x + sprite->getSpriteRect()->w/2 << ", y: " << sprite->getSpriteRect()->y + sprite->getSpriteRect()->h << endl;
        //cout << "row: " << row << ", column: " << column << endl;

        ///-------------------------------------------------------------------------------------
        int numberOfTiles = 0;
        int numberOfGrassTiles = 0;
        bool countBackgroundTiles = false;
        bool countGeneralTiles = false;
        for(int i = row; i < backgroundTiles->getLevelMatrix().size(); i++){
            ///-------------------------------------------------------------------------------------
            if(countBackgroundTiles){
                if(backgroundTiles->getLevelMatrix()[i][column] != "0"){
                    numberOfTiles++;
                    if(player != nullptr && backgroundTiles->getLevelMatrix()[i][column].find('t') != std::string::npos){
                        numberOfGrassTiles++;
                    };
                };
            };

            if(!countBackgroundTiles && i < backgroundTiles->getLevelMatrix().size()-1){
                if(backgroundTiles->getLevelMatrix()[i+1][column].find('t') != std::string::npos){
                    countBackgroundTiles = true;
                };
            };
            ///-------------------------------------------------------------------------------------


            ///-------------------------------------------------------------------------------------
            if(countGeneralTiles){
                if(generalTiles->getLevelMatrix()[i][column] != "0"){
                    numberOfTiles++;
                };
            };

            if(!countGeneralTiles && i < generalTiles->getLevelMatrix().size()-1){
                if(generalTiles->getLevelMatrix()[i+1][column].find('j') != std::string::npos){
                    countGeneralTiles = true;
                };
            };
            ///-------------------------------------------------------------------------------------
        };
        ///-------------------------------------------------------------------------------------


        ///-------------------------------------------------------------------------------------
        if(player != nullptr){
            if(column < 319 && numberOfGrassTiles > 1){
                if(backgroundTiles->getLevelMatrix()[row][column+1].find('478') != std::string::npos){
                    sprite->move(-1, 0);
                    backgroundImage->move(3, 0);
                    backgroundTiles->move(3, 0);
                    generalTiles->move(3, 0);
                    player->turnOffBackground();
                };
            };

            if(!player->getSprite()->getOnTheGround()){

                if(row > 0 && column > 0 && column < 319){

                    if(generalTiles->getLevelMatrix()[row][column+1] == "jquestion5" || generalTiles->getLevelMatrix()[row][column+1] == "n0" &&
                       generalTiles->getLevelMatrix()[row-1][column+1] == "jquestion5"){
                        sprite->move(-1, 0);
                        backgroundImage->move(3, 0);
                        backgroundTiles->move(3, 0);
                        generalTiles->move(3, 0);
                        player->turnOffBackground();
                    } else if (generalTiles->getLevelMatrix()[row][column-1] == "jquestion5" || generalTiles->getLevelMatrix()[row][column-1] == "n0" &&
                       generalTiles->getLevelMatrix()[row-1][column-1] == "jquestion5"){
                        sprite->move(1, 0);
                        backgroundImage->move(-3, 0);
                        backgroundTiles->move(-3, 0);
                        generalTiles->move(-3, 0);
                        player->turnOffBackground();
                    };

                    if(generalTiles->getLevelMatrix()[row-1][column] == "jquestion5"){
                        player->getSprite()->setState(sprite->getState()&~Sprite::UP);

                        generalTiles->changeLevel(row-1, column, "jquestion_hit");
                        player->setPoints(player->getPoints() + 100);
                        player->setCoins(player->getCoins() + 1);
                        Mix_PlayChannel(1, backgroundTiles->getSoundEffect("coin"), 0);
                    };
                };
            };
        };
        ///-------------------------------------------------------------------------------------


        ///-------------------------------------------------------------------------------------
        if(yCoordinate < 432-16*numberOfTiles){
            sprite->getSpriteRect()->y += 4;
            sprite->setOnTheGround(false);
        } else {
            sprite->setOnTheGround(true);
        };
        ///-------------------------------------------------------------------------------------
    };
};

#endif // SPRITE_H_INCLUDED
