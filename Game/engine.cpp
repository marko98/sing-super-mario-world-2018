#include "engine.h"

/*ostream& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}*/

ostream& operator<<(ostream& out, const Sprite& s) {
    out << s.spriteRect->x << " " << s.spriteRect->y << " " << s.spriteRect->w << " " << s.spriteRect->h << " " << s.onTheGround << endl;

    return out;
}

Engine::Engine(const string &gameTitle){
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
    window = SDL_CreateWindow(gameTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1280, 432, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    TTF_Init();
    font = TTF_OpenFont("Super-Mario-World.ttf", 20);
    TTF_SetFontStyle(font, TTF_STYLE_BOLD|TTF_STYLE_ITALIC);

    if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
        cerr << "Error: " << Mix_GetError() << endl;
}

Engine::~Engine(){
    Mix_FreeMusic(backgroundMusic);
    backgroundMusic = nullptr;
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = nullptr;
    renderer = nullptr;
    TTF_Quit();
    IMG_Quit();
    Mix_Quit();
    SDL_Quit();
}

void Engine::addTileset(const string &path, const string &name){
    ifstream putanja(path);
    tilesets[name] = new Tileset(putanja, renderer);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addLevel(const string &path, const string &name, int x, int y, int tileWidth, int tileHeight){
    ifstream levelTemplateStream(path);
    Level *current_level = new Level(levelTemplateStream, tilesets[name], tileWidth, tileHeight);
    current_level->addSoundEffects("resources/sounds/soundEffects.txt");
    current_level->move(x, y);
    addDrawable(current_level);
    addMovable(current_level);
}

void Engine::addPlayer(const string &spriteSheatPath, int frameSkip, Engine *engine){
    ifstream spriteSheetStream(spriteSheatPath);
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sprite = new Sprite(sheet);
    sprite->setFrameSkip(frameSkip);

    Player *player = new Player(sprite, engine);
    Gravity *gravity =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), dynamic_cast<Level*>(drawables[2]), player);

    addDrawable(player);
    addMovable(player);
    eventListeners.push_back(player);
    addGravitable(gravity);

    leadPlayer = player;

    // VEZIVANJE BACKGROUND IMAGE ZA GLAVNOG PLAYERA
    player->setBackgroundImage(dynamic_cast<Level*>(drawables[0]));
    // VEZIVANJE GLAVNOG PLAYERA ZA BACKGROUND IMAGE
    dynamic_cast<Level*>(drawables[0])->setPlayer(player);

    // VEZIVANJE GLAVNOG PLAYERA ZA BACKGROUND TILES
    player->setBackgroundTiles(dynamic_cast<Level*>(drawables[1]));
    // VEZIVANJE BACKGROUND TILES ZA GLAVNOG PLAYERA
    dynamic_cast<Level*>(drawables[1])->setPlayer(player);

    // VEZIVANJE GLAVNOG PLAYERA ZA GENERAL TILES
    player->setGeneralTiles(dynamic_cast<Level*>(drawables[2]));
    // VEZIVANJE GENERAL TILES ZA GLAVNOG PLAYERA
    dynamic_cast<Level*>(drawables[2])->setPlayer(player);
}

void Engine::addEnemy(const string &spriteSheatPath, int frameSkip, int x){
    ifstream spriteSheetStream(spriteSheatPath);
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sprite = new Sprite(sheet, x);
    sprite->setFrameSkip(frameSkip);

    Enemy *enemy = new Enemy(sprite);

    Gravity *gravity =  new Gravity (dynamic_cast<Level*>(drawables[0]), dynamic_cast<Level*>(drawables[1]), dynamic_cast<Level*>(drawables[2]), sprite);

    addDrawable(enemy);
    addMovable(enemy);
    addEnemies(enemy);
    addGravitable(gravity);

    // VEZIVANJE BACKGROUND IMAGE ZA ENEMY
    enemy->setBackgroundImage(dynamic_cast<Level*>(drawables[0]));
}

void Engine::addCoin(const string &spriteSheatPath, int frameSkip, int x, int y){
    ifstream spriteSheetStream(spriteSheatPath);
    SpriteSheet *sheet = new SpriteSheet(spriteSheetStream, renderer);

    Sprite *sprite = new Sprite(sheet, x, y);
    sprite->setFrameSkip(frameSkip);

    Coin *coin = new Coin(sprite);

    addDrawable(coin);
}

void Engine::addBackgroundMusic(string backgroundMusicPath){
    backgroundMusic = Mix_LoadMUS(backgroundMusicPath.c_str());// ako je duze od 10 sek
}

void Engine::addDrawable(Drawable* drawable){
    drawables.push_back(drawable);
}

void Engine::addMovable(Movable* movable){
    movables.push_back(movable);
}

void Engine::addGravitable(Gravity* gravitable){
    gravitables.push_back(gravitable);
}

void Engine::addEnemies(Enemy* enemy){
    enemies.push_back(enemy);
}

void Engine::drawTextOnWindow(){
    //ISPIS NA EKRAN----------------------------------------------------------------
    //LEVA STRANA -------------------------------------------------------
    drawText("Mario", 40, 20, font, renderer, 255, 0, 0, 20, 100);

    drawText("x", 60, 45, font, renderer, 255, 255, 255, 15, 15);

    stringstream stringHealth;
    stringHealth << leadPlayer->getHealth();
    drawText(stringHealth.str(), 80, 42, font, renderer, 255, 255, 255, 18, 18);

    drawText("Enemies", 160, 20, font, renderer, 255, 255, 255, 20, 100);
    stringstream stringEnemiesSize;
    stringEnemiesSize << enemies.size();
    drawText(stringEnemiesSize.str(), 200, 42, font, renderer, 255, 255, 255, 18, 18);

    //DESNA STRANA -------------------------------------------------------
    drawText("TIME", 1000, 20, font, renderer, 255, 165, 0, 15, 80);

    time_t currentTime;
    if(enemies.size() != 0 && leadPlayer->getHealth() != 0){
        currentTime = time (NULL) - start;
        timeLeft = time (NULL) - start;
    };
    stringstream stringTime;
    if(150 - currentTime <= 0){
        if(Mix_PlayingMusic()){
            Mix_PauseMusic();
        };
        drawText("GAME OVER", 520, 192, font, renderer, 255, 255, 255, 48, 240);
        finishGame();
    } else {
        stringTime << 150 - timeLeft;
        drawText(stringTime.str(), 1050, 40, font, renderer, 255, 255, 255, 30, 30);
    };

    drawText("x", 1175, 20, font, renderer, 255, 255, 255, 15, 15);
    stringstream stringCoins;
    stringCoins << leadPlayer->getCoins();
    drawText(stringCoins.str(), 1200, 18, font, renderer, 255, 255, 255, 20, 18);

    stringstream stringPoints;
    stringPoints << leadPlayer->getPoints();
    drawText(stringPoints.str(), 1200, 42, font, renderer, 255, 255, 255, 32, 32);

    if(leadPlayer->getHealth() == 0){
        if(Mix_PlayingMusic()){
            Mix_PauseMusic();
        };
        drawText("GAME OVER", 520, 192, font, renderer, 255, 255, 255, 48, 240);
    } else if(enemies.size() == 0){
        if(Mix_PlayingMusic()){
            Mix_PauseMusic();
        };
        drawText("YOU ARE A WINNER", 520, 192, font, renderer, 255, 255, 255, 48, 240);
    };
    //--------------------------------------------------------------------------------
}

void Engine::drawText(string text, int x, int y, TTF_Font* eFont, SDL_Renderer* eRenderer, int r, int g, int b, int height, int width){
    SDL_Color color = {r, g, b};

    stringstream ss;
    ss << text ;
    SDL_Surface *surface = nullptr;
    SDL_Texture *texture = nullptr;

    surface = TTF_RenderText_Solid(eFont, ss.str().c_str(), color);
    texture = SDL_CreateTextureFromSurface(eRenderer, surface);
    SDL_Rect message;
    message.x = x;
    message.y = y;
    message.h = height;
    message.w = width;
    SDL_FreeSurface(surface);

    SDL_RenderCopy(eRenderer, texture, NULL, &message);
}

void Engine::finishGame(){
    for(size_t i = 0; i < eventListeners.size(); i++) {
        if(eventListeners[i] == leadPlayer){
            eventListeners.erase(eventListeners.begin()+i);
        };
    };
    for(size_t i = 0; i < gravitables.size(); i++) {
        if(gravitables[i]->getSprite() == leadPlayer->getSprite()){
            gravitables.erase(gravitables.begin()+i);
        };
    };
    for(size_t i = 0; i < movables.size(); i++) {
        if(movables[i] == leadPlayer){
            movables.erase(movables.begin()+i);
        };
    };
    for(size_t i = 0; i < drawables.size(); i++) {
        if(drawables[i] == leadPlayer){
            drawables.erase(drawables.begin()+i);
        };
    };
}

bool Engine::run(){
    Mix_PlayMusic(backgroundMusic, 0);
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    SDL_Event event;

    start = time (NULL);
    while(!quit){
        if(leadPlayer->getHealth() == 0){
            finishGame();
        };
        //cout << *(leadPlayer->getSprite()) << endl;
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_ESCAPE){
                quit = true;
            } else if(event.key.keysym.sym == SDLK_p){
                if(Mix_PlayingMusic()){
                    Mix_PauseMusic();
                };
            } else if(event.key.keysym.sym == SDLK_r){
                if(Mix_PausedMusic()){
                    Mix_ResumeMusic();
                };
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++){
                    eventListeners[i]->listen(event);
                };
            };
        };

        for(size_t i = 0; i < gravitables.size(); i++) {
            gravitables[i]->gravity();
        };

        for(size_t i = 0; i < movables.size(); i++) {
            movables[i]->move();
        };

        for(size_t i = 0; i < enemies.size(); i++) {
            enemies[i]->randomChangeStates();
            if(leadPlayer->getHealth() > 0){
                leadPlayer->attack(enemies[i]);
            };
            enemies[i]->attack(leadPlayer);
        };


        SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        };
        drawTextOnWindow();

        SDL_RenderPresent(renderer);

        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay){
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        };
    };
}
