#include "spritesheet.h"

SpriteSheet::SpriteSheet(istream &inputStream, SDL_Renderer *renderer){
    string path;
    string animation;
    int totalFrames;

    inputStream >> path;
    SDL_Surface *surface = IMG_Load(path.c_str());
    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    while(!inputStream.eof()){
        inputStream >> animation;
        animations[animation] = Frames();
        inputStream >> totalFrames;
        for(int i = 0; i < totalFrames; i++){
            animations[animation].push_back(new Frame(inputStream));
        };
    };
}

SpriteSheet::~SpriteSheet(){
    SDL_DestroyTexture(texture);
    animations.clear();
    //delete animations;
}

void SpriteSheet::drawFrame(string animation, int frame, SDL_Rect *dest, SDL_Renderer *renderer){
    int deltaX = (dest->w - animations[animation][frame]->getRect()->w)/2;
    int deltaY = (dest->h - animations[animation][frame]->getRect()->h);

    SDL_Rect renderQuad;
    renderQuad.x = dest->x + deltaX;
    renderQuad.y = dest->y + deltaY;
    renderQuad.h = animations[animation][frame]->getRect()->h;
    renderQuad.w = animations[animation][frame]->getRect()->w;

    SDL_RenderCopy(renderer, texture, animations[animation][frame]->getRect(), &renderQuad);
}
