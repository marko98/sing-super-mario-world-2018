#include "level.h"
#include "player.h"
#include "sprite.h"

Level::Level(istream &inputStream, Tileset *tileset, int levelTileWidth, int levelTileHeight): Drawable(){
    this->x = 0;
    this->y = 0;
    this->tileset = tileset;
    this->levelTileWidth = levelTileWidth;
    this->levelTileHeight = levelTileHeight;
    this->state = Level::STOP;
    int rows, columns;
    string tileCode;
    inputStream >> rows >> columns;
    for(int i = 0; i < rows; i++){
        level.push_back(LevelRow());
        for(int j = 0; j < columns; j++){
            inputStream >> tileCode;
            level[i].push_back(tileCode);
        }
    }
}

Level::~Level(){
    delete tileset;
}

int Level::getX(){
    return x;
}

void Level::setX(int x){
    this->x = x;
}

bool Level::getMoving(){
    return moving;
}

void Level::setMoving(bool moving){
    this->moving = moving;
}

short int Level::getState(){
    return state;
}

void Level::setState(short int state){
    this->state = state;
}

void Level::changeLevel(int row, int column, const string &name){
    level[row][column] = name;
}

void Level::addSoundEffects(const string &soundEffectsPath){
    ifstream soundEffectsPathStream(soundEffectsPath);

    string soundEffectName;
    string soundEffectSource;

    //cout << soundEffectName << soundEffectSource << endl;
    while(!soundEffectsPathStream.eof()){
        soundEffectsPathStream >> soundEffectName;
        soundEffectsPathStream >> soundEffectSource;
        Mix_Chunk *soundEffect = Mix_LoadWAV(soundEffectSource.c_str());
        soundEffects[soundEffectName] = soundEffect;
    };

}

Mix_Chunk* Level::getSoundEffect(string name){
    return soundEffects[name];
}

void Level::move(int dx, int dy){
    x += dx;
    y += dy;
}

void Level::move(){
    if(state != STOP){
        if(state&LEFT && x > -3840){
            if(player->getSprite()->getState() != Sprite::RIGHT_DOWN && player->getSprite()->getState() != Sprite::RIGHT_UP){
                move(-3, 0);
            };
        };
        if(state&RIGHT && x < 0){
            if(player->getSprite()->getState() != Sprite::LEFT_DOWN && player->getSprite()->getState() != Sprite::LEFT_UP){
                move(3, 0);
            };
        }

        if(x == 0 || x == -3840){
            player->turnOffBackground();
        };
    };
}

void Level::setPlayer(Player *player){
    this->player = player;
}

Player * Level::getPlayer(){
    return player;
}

const Level::LevelMatrix & Level::getLevelMatrix() const {
    return level;
}

const Level::LevelRow & Level::getLevelRow(int row) const {
    return level[row];
}

void Level::draw(SDL_Renderer *renderer){
    for(size_t i = 0; i < level.size(); i++) {
        for(size_t j = 0; j < level[i].size(); j++) {
            tileset->draw(level[i][j], x+j*levelTileWidth, y+i*levelTileHeight, renderer);
        }
    }
}
