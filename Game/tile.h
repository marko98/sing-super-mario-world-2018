#ifndef TILE_H_INCLUDED
#define TILE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using namespace std;

class Tile{
private:
    SDL_Rect *tileRect;
public:
    Tile(int x, int y, int w, int h);
    Tile(istream &inputStream);
    virtual ~Tile();

    SDL_Rect * getRect();
    int x();
    int y();
    int w();
    int h();
};

#endif // TILE_H_INCLUDED
