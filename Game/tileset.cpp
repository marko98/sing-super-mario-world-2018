#include "tileset.h"

Tileset::Tileset(istream &inputStream, SDL_Renderer *renderer){
    string path;
    inputStream >> path;

    SDL_Surface *surface = IMG_Load(path.c_str());

    if(path == "resources/tilesets/ground_tiles.png"){
        SDL_SetColorKey(surface, SDL_TRUE, SDL_MapRGB(surface->format, 37, 65, 82));
    }

    texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);

    while(!inputStream.eof()){
        string code;
        inputStream >> code;
        tileset[code] = new Tile(inputStream);
    }
    dest = new SDL_Rect;
}

Tileset::~Tileset(){
    tileset.clear();
    SDL_DestroyTexture(texture);
    delete dest;
}

map<string, Tile*> Tileset::getTileset(){
    return tileset;
}

void Tileset::draw(string code, int x, int y, SDL_Renderer *renderer){
    dest->x = x;
    dest->y = y;
    dest->w = tileset[code]->getRect()->w;
    dest->h = tileset[code]->getRect()->h;
    SDL_RenderCopy(renderer, texture, tileset[code]->getRect(), dest);
}
