#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <stdio.h>
#include <time.h>
#include <iostream>
#include <sstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>

#include "tileset.h"
#include "drawable.h"
#include "movable.h"
#include "spritesheet.h"
#include "sprite.h"
#include "enemy.h"
#include "eventlistener.h"
#include "player.h"
#include "level.h"

using namespace std;

struct Coin;

class Engine{
public:
    typedef map<string, Tileset*> Tilesets;
    typedef vector<Drawable*> Drawables;
    typedef vector<Movable*> Movables;
    typedef vector<EventListener*> EventListeners;
    typedef vector<Gravity*> Gravitables;
    typedef vector<Enemy*> Enemies;
    typedef vector<Coin*> Coins;
    Drawables drawables;
    Movables movables;
    Enemies enemies;
    Gravitables gravitables;
private:
    Tilesets tilesets;
    EventListeners eventListeners;
    Coins coins;

    Player *leadPlayer;
    SDL_Window *window;
    SDL_Renderer *renderer;
    TTF_Font *font;
    Mix_Music *backgroundMusic;

    int frameCap = 60;
    time_t start;
    bool quit = false;
    time_t timeLeft;
public:
    Engine(const string &gameTitle);
    virtual ~Engine();

    void drawText(string text, int x, int y, TTF_Font* eFont, SDL_Renderer* eRenderer, int r, int g, int b, int width, int height);
    void drawTextOnWindow();
    void finishGame();
    bool run();

    void addBackgroundMusic(string backgroundMusicPath);
    void addTileset(const string &path, const string &name);
    Tileset* getTileset(const string &name);
    void addLevel(const string &path, const string &name, int x, int y, int tileWidth, int tileHeight);
    void addEnemy(const string &spriteSheatPath, int frameSkip, int x=0);
    void addPlayer(const string &spriteSheatPath, int frameSkip, Engine *engine);
    void addCoin(const string &spriteSheatPath, int frameSkip, int x=0, int y=0);
    void addDrawable(Drawable* drawable);
    void addMovable(Movable* movable);
    void addGravitable(Gravity* gravitable);
    void addEnemies(Enemy* enemy);
};

struct Coin: public Drawable{
    Sprite *sprite;

    Coin(Sprite *sprite): Drawable(), sprite(sprite){};
    virtual void draw(SDL_Renderer *renderer){
        sprite->getSpriteSheet()->drawFrame("turn", sprite->getCurrentFrame(), sprite->getSpriteRect(), renderer);

        sprite->setFrameCounter(sprite->getFrameCounter() + 1);
        if(sprite->getFrameCounter()%sprite->getFrameSkip() == 0){
            sprite->setCurrentFrame(sprite->getCurrentFrame() + 1);
            if(sprite->getCurrentFrame() >= 4){
                sprite->setCurrentFrame(0);
            };
            sprite->setFrameCounter(0);
        };

    };
};

#endif // ENGINE_H_INCLUDED
