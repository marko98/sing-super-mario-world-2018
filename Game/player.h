#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"
#include "level.h"
#include "enemy.h"

class Sprite;
class Engine;

using namespace std;

class Player: public Drawable, public Movable, public KeyboardEventListener{
private:
    Sprite *sprite;
    int row, column;
    bool dvostrukiSkok = false;
    int brojacSkoka = 0;
    Level *backgroundImage;
    Level *backgroundTiles;
    Level *generalTiles;
    int health, coins, points;
    Engine *engine;
public:
    Player(Sprite *sprite, Engine *engine);
    virtual ~Player();
    virtual void draw(SDL_Renderer *renderer);
    virtual void move(int dx, int dy);
    virtual void move();
    virtual void listen(SDL_KeyboardEvent &event);

    Sprite * getSprite();
    int getHealth();
    void setHealth(int health);
    int getCoins();
    void setCoins(int coins);
    int getPoints();
    void setPoints(int points);

    void setBackgroundImage(Level *backgroundImage);
    void setBackgroundTiles(Level *backgroundTiles);
    void setGeneralTiles(Level *generalTiles);
    void moveBackgroundToLeft();
    void moveBackgroundToRight();
    void dontMoveBackground();
    void turnOnBackground();
    void turnOffBackground();
    void attack(Enemy *enemy);
};

#endif // PLAYER_H_INCLUDED
