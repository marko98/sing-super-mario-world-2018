#ifndef LEVEL_H_INCLUDED
#define LEVEL_H_INCLUDED

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "drawable.h"
#include "movable.h"
#include "tileset.h"

class Player;

using namespace std;

class Level : public Drawable, public Movable{
public:
    typedef vector<string> LevelRow;
    typedef vector<LevelRow > LevelMatrix;
    typedef map<string, Mix_Chunk*> SoundEffects;
    enum State: short int{STOP=0, LEFT=1, RIGHT=2};
private:
    short int state;
    Player *player;
    Tileset *tileset;
    LevelMatrix level;
    int levelTileWidth, levelTileHeight, x, y;
    bool moving = false;
    SoundEffects soundEffects;
public:
    Level(istream &inputStream, Tileset *tileset, int levelTileWidth, int levelTileHeight);
    virtual ~Level();

    void setPlayer(Player *player);
    Player * getPlayer();

    const LevelMatrix & getLevelMatrix() const;
    const LevelRow & getLevelRow(int row) const;
    int getX();
    void setX(int x);
    void addSoundEffects(const string &soundEffectsPath);
    Mix_Chunk* getSoundEffect(string name);
    bool getMoving();
    void setMoving(bool moving);

    virtual void draw(SDL_Renderer *renderer);
    virtual void move();
    virtual void move(int dx, int dy);
    short int getState();
    void setState(short int state);
    void changeLevel(int row, int column, const string &name);

    //friend ostream& operator<<(ostream&, const Level&);
    friend ostream& operator<<(ostream& out, const Level& l){
        int rows = l.getLevelMatrix().size();
        int cols = 0;
        if(rows > 0) {
            cols = l.getLevelMatrix()[0].size();
        }
        out << rows << " " << cols << endl;

        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++) {
                out << l.getLevelMatrix()[i][j] << " ";
            }
            out << endl;
        }

        return out;
    };
};

#endif // LEVEL_H_INCLUDED
